package com.challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class GetExchangeRateRsp {
	
	private Double amount;
	private Double amountExchangeRate;
	private String currencyOrigin;
	private String destinationCurrency;
	private Double exchangeRate;
	

}
