package com.challenge.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class GetExchangeRateReq {
	
	@NotNull(message = "The 'amount' field is required")
	private Double amount;
	
	@NotNull(message = "'currencyOrigin' is required")
	@NotBlank(message = "The value of 'currencyOrigin' is mandatory")
	@Pattern(regexp = "^(PEN|USD)+$", message = "The 'currencyOrigin' field can only contain USD or PEN")
	private String currencyOrigin;
	
	@NotNull(message = "The 'destinationCurrency' field is required")
	@NotBlank(message = "The value of 'destinationCurrency' is mandatory")
	@Pattern(regexp = "^(PEN|USD)+$", message = "The 'destinationCurrency' field can only contain USD or PEN")
	private String destinationCurrency;

}
