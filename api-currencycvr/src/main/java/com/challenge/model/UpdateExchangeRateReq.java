package com.challenge.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class UpdateExchangeRateReq {	
	
	@NotNull(message = "The 'name' field is required")
	@NotBlank(message = "The value of 'name' is mandatory")
	@Pattern(regexp = "^(PEN|USD)+$", message = "The 'name' field can only contain USD or PEN")
	private String name;
	
	@NotNull(message = "The 'value' field is required")
	private Double value;

}