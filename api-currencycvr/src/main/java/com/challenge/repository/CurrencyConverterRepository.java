package com.challenge.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challenge.entity.CurrencyConverter;

public interface CurrencyConverterRepository extends JpaRepository<CurrencyConverter, Long>{
	
	Optional<CurrencyConverter> findByName(String name);

}
