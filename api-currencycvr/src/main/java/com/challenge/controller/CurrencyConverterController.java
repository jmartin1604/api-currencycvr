package com.challenge.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.model.AddExchangeRateReq;
import com.challenge.model.GetExchangeRateReq;
import com.challenge.model.GetExchangeRateRsp;
import com.challenge.model.UpdateExchangeRateReq;
import com.challenge.service.CurrencyConverterService;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/v1/currency", produces = "application/json")
public class CurrencyConverterController {

	@Autowired
	private CurrencyConverterService currencyCvrService;
	
	@ApiOperation(value = "Operación que permite agregar el tipo de cambio de Soles a Dólares.")
	@PostMapping(value = "/insert/exchange/rate")
	public Completable addCurrencyConverter(@Valid @RequestBody AddExchangeRateReq request) {
		return this.currencyCvrService.addExchangeRate(request);
	}

	@ApiOperation(value = "Operación que permite aplicar el tipo de cambio de Soles a Dólares.")
	@PostMapping(value = "/get/exchange/rate")
	public Single<ResponseEntity<GetExchangeRateRsp>> getExchangeRate(@Valid @RequestBody GetExchangeRateReq request) {
		return this.currencyCvrService.getExchangeRate(request)
				.map(getCurCvrReactive -> new ResponseEntity<>(getCurCvrReactive, HttpStatus.OK));
	}

	@ApiOperation(value = "Operación que permite actualizar el tipo de cambio de Soles a Dólares.")
	@PostMapping(value = "/update/exchange/rate")
	public Single<ResponseEntity<String>> updateExchangeRate(@Valid @RequestBody UpdateExchangeRateReq request) {
		return this.currencyCvrService.updateExchangeRate(request).subscribeOn(Schedulers.io())
				.toSingle(() -> ResponseEntity.ok(null));
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}

}
