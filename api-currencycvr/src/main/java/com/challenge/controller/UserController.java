package com.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.model.UserRsp;
import com.challenge.service.AuthenticationService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/v1/currency", produces = "application/json")
public class UserController {
	
	@Autowired
	private AuthenticationService authenticationService;

	@ApiOperation(value = "Operación que permite generar un token de acceso.")
	@PostMapping(value = "/security/user")
	public UserRsp login(@RequestParam("user") String username, @RequestParam("password") String pwd) {
		return UserRsp.builder().user(username).token(authenticationService.getJWTToken(username)).build();

	}
}