package com.challenge.service;

import com.challenge.model.AddExchangeRateReq;
import com.challenge.model.GetExchangeRateReq;
import com.challenge.model.GetExchangeRateRsp;
import com.challenge.model.UpdateExchangeRateReq;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

public interface CurrencyConverterService {
	
	public Completable addExchangeRate(AddExchangeRateReq request);
	public Single<GetExchangeRateRsp> getExchangeRate(GetExchangeRateReq request);
	public Completable updateExchangeRate(UpdateExchangeRateReq request);	

}
