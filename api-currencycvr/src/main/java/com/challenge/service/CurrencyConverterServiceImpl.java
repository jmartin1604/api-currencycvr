package com.challenge.service;

import java.text.DecimalFormat;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.entity.CurrencyConverter;
import com.challenge.model.AddExchangeRateReq;
import com.challenge.model.GetExchangeRateReq;
import com.challenge.model.GetExchangeRateRsp;
import com.challenge.model.UpdateExchangeRateReq;
import com.challenge.repository.CurrencyConverterRepository;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

@Service
public class CurrencyConverterServiceImpl implements CurrencyConverterService {

	@Autowired
	private CurrencyConverterRepository repository;
	
	public Completable addExchangeRate(AddExchangeRateReq request) {
		return Completable.create(completableSubscriber -> {
			Optional<CurrencyConverter> optionalCurrency = repository.findByName(request.getName());
			if (!optionalCurrency.isPresent()) {
				repository.save(toCurrrencyConverter(request));
				completableSubscriber.onComplete();
			} else {
				completableSubscriber.onError(new Throwable("Error al insertar .."));
			}
		});
	}
	
	private CurrencyConverter toCurrrencyConverter(AddExchangeRateReq addExchangeRateReq) {
		CurrencyConverter currencyCvr = new CurrencyConverter();
        BeanUtils.copyProperties(addExchangeRateReq, currencyCvr);
        return currencyCvr;
    }

	@Override
	public Single<GetExchangeRateRsp> getExchangeRate(GetExchangeRateReq request) {
		return Single.fromCallable(() -> repository.findByName(request.getDestinationCurrency()))
				.map(resultRepository -> {
					 DecimalFormat df = new DecimalFormat("#.00");
					return GetExchangeRateRsp.builder().amount(request.getAmount())
							.amountExchangeRate(
									Double.parseDouble(df.format(request.getDestinationCurrency().equals("USD")
											? request.getAmount() * resultRepository.get().getValue()
											: request.getAmount() / resultRepository.get().getValue())))
							.destinationCurrency(request.getDestinationCurrency())
							.exchangeRate(resultRepository.get().getValue()).build();
		});
	}

	public Completable updateExchangeRate(UpdateExchangeRateReq request) {
		return Completable.create(completableSubscriber -> {
			Optional<CurrencyConverter> optionalCurrency = repository.findByName(request.getName());
			if (!optionalCurrency.isPresent())
				completableSubscriber.onError(new EntityNotFoundException());
			else {
				CurrencyConverter currencyCvr = optionalCurrency.get();
				currencyCvr.setValue(request.getValue());
				repository.save(currencyCvr);
				completableSubscriber.onComplete();
			}
		});
	}
}
