package com.challenge.service;

public interface AuthenticationService {
	
	public String getJWTToken(String username);

}
